# FootSwitch Arduino via MIDI/USB for IPad - AKA Milky-Switch

## Objective

To have a footswitch to operate Amplitube on IPAD using MIDI over USB

![alt text](https://bitbucket.org/jadelabsdev/footswitch/raw/f1c467035939c320e4806c90c0870c7725fab9ba/images/finished.png)

Guitar -> IRig -> TRRS -> IPAD -> TRRS -> IRig -> Speakers

FootSwitch -> USB -> Lightining Adapter -> IPAD


The footswitch will be presented as a MIDI Device to the IPAD and we can map Amplitube to respond to the MIDI commands, turning on and off effects and interacting with the Expression Pedal.

## DISCLAIMER
It worked successfully with Arduino Micro 32U4. The references for Arduino Uno were not tested but they are here to help you on your chase if you are planning to put an Uno to do the trick. Good luck!

## CREDITS
During the research I got in touch with the material from **Mr. Gustavo Silveira**. Here is the acknowledgement about all the help his material provided.


You can find his research at:

- http://www.musiconerd.com
- http://www.bitcontrollers.com

**Mr. Gustavo, thank you very much!**

## Hardware

### Controler related
- Arduino Micro 32U4
- [FTDI Adapter](https://www.amazon.de/gp/product/B01N9RZK6I/ref=oh_aui_search_asin_title?ie=UTF8&psc=1) to program Arduino - **NOT NEEDED FOR ARDUINO MICRO**

### Sketch

Schematics available [here](./images/MidiDevice_schem.png)

### Interface with Ipad Mini

The idea of having this over USB and not Bluetooth is to avoid interference in the studio, when a lot of things are connected at the surroundings.

[Something like this adapter](https://www.amazon.de/s?k=Lightning-Adapter) will be needed. 

## Deployment instructions

### Firmware to allow MIDI over USB - **NOT NEEDED FOR ARDUINO MICRO**
In order to have MIDI over USB and therefore a MIDI compliant hardware presented to the consumer, we need to override the firmware for the MEGA 8U2 chip. The binary is available at the bin directory and a new one can be generated [Here](https://moco-lufa-web-client.herokuapp.com/#/)

### Pin mapping between FTDI and UNO - **NOT NEEDED FOR ARDUINO MICRO**
FTDI    -  UNO

GND -> GND

CTS -> not connected!

5V  -> 5V

TXD ->RX (D0)

RXD ->TX (D1)

DTR -> GND    


During upload the auto-reset-function is not working, though you need to press the reset button on the arduino. I didn't figure out when exactly the button needs to be pushed. For me it worked well when I pushed the reset button when the RX-Diode on the FTDI-break out is blinking. By the way the wireconnection worked as well for the arduino pro mini.

OR: DTR -> 100nanoFahrrad capacitor  -> RESET (on Arduino)  then the auto-reset function is given!!

## Tweaks at arduino code

### To change the name of your arduino device
To change the name that will be presented when your arduino is connected to the IPAD / Mac, you need to change the boards.txt in your arduino installation. 

In my case this is at /Users/USERNAME/Library/Arduino15/packages/arduino/hardware/avr/1.6.23/boards.txt. Look for:
    
    - "micro.build.board=AVR_MICRO"

Change the line above this one with the name you want:
    
    - micro.build.usb_product="Milky Switch"
 
### To Fix the problem of "Cannot use the device The connected device requires too much power."

When I first tried to connect to the IPad Mini I got this message and the device wasn't available. To workaround this I editted the file under: /Users/USERNAME/Library/Arduino15/packages/arduino/hardware/avr/1.6.23/cores/arduino/USBCore.h. There we have a configuration defining how much energy Arduino will request from USB. I read somewhere that the maximum allowed value is 100ma. 

Original: 

\#define D_CONFIG(_totalLength,_interfaces) \
	{ 9, 2, _totalLength,_interfaces, 1, 0, USB_CONFIG_BUS_POWERED | USB_CONFIG_REMOTE_WAKEUP, USB_CONFIG_POWER_MA(500) }

Changed to 100:

\#define D_CONFIG(_totalLength,_interfaces) \
	{ 9, 2, _totalLength,_interfaces, 1, 0, USB_CONFIG_BUS_POWERED | USB_CONFIG_REMOTE_WAKEUP, USB_CONFIG_POWER_MA(100) }

## References

### Tested reference to use with Arduino Micro:
https://www.youtube.com/watch?v=32myesnWLuY&list=PLDvka7NV8VPC_J40TdxirbjC-LYEZ5-rg&index=2&t=30s


### Untested References to use Arduino Uno:
https://www.arduino.cc/en/Tutorial/MidiDevice

http://morecatlab.akiba.coocan.jp/morecat_lab/MocoLUFA.html

http://morecatlab.akiba.coocan.jp/lab/index.php/aruino/midi-firmware-for-arduino-uno-moco/?lang=en

https://moco-lufa-web-client.herokuapp.com/#/

https://www.arduino.cc/en/Hacking/MidiWith8U2Firmware

https://www.musiconerd.com/single-post/build-this-midi-controller-the-transport

https://github.com/silveirago/the-transport

https://www.amazon.de/Micro-ATmega32U4-Arduino-Leonardo-%C3%A4hnlich/dp/B01D0OI90U/ref=sr_1_2_sspa?ie=UTF8&qid=1551365116&sr=8-2-spons&keywords=arduino+pro+micro&psc=1

https://forum.arduino.cc/index.php?topic=452226.0

https://github.com/tttapa/MIDI_controller

https://www.youtube.com/watch?v=U9kDIyIgHNk
