/*
  Made by Gustavo Silveira, 2016.
  This Sketch reads the Arduino's digital and analog ports and send midi notes and midi control change

  http://www.musiconerd.com
  http://www.bitcontrollers.com

  If you are using for anything that's not personal use don't forget to give credit.

  gustavosilveira@musiconerd.com

  Added support for steady buttons - Roberto Leite 2019 - roberto.leite.br@gmail.com
*/


#include "MIDIUSB.h"

/////////////////////////////////////////////
// buttons
const int modePin = 6;                    // controls if the behaviour for the buttons is push button or press on - press off
const int NButtons = 4;                   // the amount of the pushbuttons
const int buttonPin[NButtons] = {2,3,4,5};    // the number of the pushbutton pins in the desired order
int buttonCState[NButtons] = {0};         // stores the button current value
int buttonPState[NButtons] = {0};         // stores the button previous value
int buttonFlipFlop[NButtons] = {0};       // stores state of on-off if mode is on-off and not pushbutton
//byte pin13index = 3;                    // put the index of the pin 13 in the buttonPin[] if you are using it, if not, comment lines 68-70


/////////////////////////////////////////////
// debounce
unsigned long lastDebounceTime[NButtons] = {0};  // the last time the output pin was toggled
unsigned long debounceDelay = 5;    //* the debounce time; increase if the output flickers

/////////////////////////////////////////////
// potentiometers
const int NPots = 1;            // amount of pots
int potPin[NPots] = {A0};   // pin where the potentiometer is
int potCState[NPots] = {0};     // current state of the pot
int potPState[NPots] = {0};     // previous state of the pot
int potVar = 0;                 // difference between the current and previous state of the pot

int midiCState[NPots] = {0}; // Current state of the midi value
int midiPState[NPots] = {0}; // Previous state of the midi value

int TIMEOUT = 300;                //* Amount of time the potentiometer will be read after it exceeds the varThreshold
int varThreshold = 1;            //* Threshold for the potentiometer signal variation
boolean potMoving = true;         // If the potentiometer is moving
unsigned long PTime[NPots] = {0}; // Previously stored time
unsigned long timer[NPots] = {0}; // Stores the time that has elapsed since the timer was reset

/////////////////////////////////////////////

byte midiCh = 0;  //* MIDI channel to be used
byte note = 33;   //* Lowest note to be used
byte cc = 50;      //* Lowest MIDI CC to be used in potentiometers signal
int mode = 0;     // LOW for push button - HIGH for steady on-off button (pedal bypass on amplitube)

int pot;
int mapped;

void setup() {


  pinMode(modePin, INPUT_PULLUP);
  pinMode(7,OUTPUT); //led indicating system on
  pinMode(8,OUTPUT); //led indicating mode (push button / steady state)
  
  for (int i = 0; i < NButtons; i++) {
    pinMode(buttonPin[i], INPUT_PULLUP);
  }
  for (int i = 0; i < NPots; i++) {
    pinMode(potPin[i], INPUT_PULLUP);
  }
  //pinMode(buttonPin[3], INPUT); //pin 13

}

void loop() {

  check_mode();
  buttons();
  potentiometers();
  digitalWrite(8, HIGH);


}
int mode_pressed = 0;

void check_mode(){
  // Amplitube reacts differently to the buttons: 
  // if you'e controlling a preset, a push button works. 
  // To control a pedal button we need one push to turn the effect on, one push to turn the effect off. That's why we need 2 modes
  
  if (digitalRead(modePin) == HIGH)
  {
    if (mode_pressed ==1){
      mode_pressed = 0;
      if (mode == LOW)
        mode = HIGH;
      else 
        mode = LOW; 
    }
  }
  else
     mode_pressed = 1;
  digitalWrite(7, mode);
  
}
/////////////////////////////////////////////
// BUTTONS
void buttons() {

  for (int i = 0; i < NButtons; i++) {

    buttonCState[i] = digitalRead(buttonPin[i]);
    /*
        // Comment this if you are not using pin 13...
        if (i == pin13index) {
          buttonCState[i] = !buttonCState[i]; //inverts pin 13 because it has a pull down resistor instead of a pull up
        }
        // ...until here
    */
    if ((millis() - lastDebounceTime[i]) > debounceDelay) {

      if (buttonPState[i] != buttonCState[i]) {
        lastDebounceTime[i] = millis();
        if (mode == LOW){ // push button mode - default
          if (buttonCState[i] == HIGH ) {
            controlChange(0, note+i+mode*10,127);
            MidiUSB.flush();
          }
          else {
            controlChange(0, note+i+mode*10,0);
            MidiUSB.flush();
          }
        }
        else { // mode is steady on-off
          if (buttonCState[i] == LOW ){
            // invert button flip flop state
            if (buttonFlipFlop[i] == 1)
              buttonFlipFlop[i] = 0;
            else
              buttonFlipFlop[i] = 1;
          }
          controlChange(0, note+i+mode*10,127 * buttonFlipFlop[i]); // sends 0 or 127
		      digitalWrite(8,buttonFlipFlop[i] );
          MidiUSB.flush();
        }
        buttonPState[i] = buttonCState[i];
      }
    }
  }
}

/////////////////////////////////////////////
// POTENTIOMETERS
void potentiometers() {

  for (int i = 0; i < NPots; i++) { // Loops through all the potentiometers

    potCState[i] = analogRead(potPin[i]); // Reads the pot and stores it in the potCState variable
    Serial.println(potCState[i]);
    midiCState[i] = map(potCState[i], 42, 273, 0, 127); // Maps the reading of the potCState to a value usable in midi


    potVar = abs(potCState[i] - potPState[i]); // Calculates the absolute value between the difference between the current and previous state of the pot

    if (potVar > varThreshold) { // Opens the gate if the potentiometer variation is greater than the threshold
      PTime[i] = millis(); // Stores the previous time
    }

    timer[i] = millis() - PTime[i]; // Resets the timer 11000 - 11000 = 0ms

    if (timer[i] < TIMEOUT) { // If the timer is less than the maximum allowed time it means that the potentiometer is still moving
      potMoving = true;
    }
    else {
      potMoving = false;
    }

    if (potMoving == true) { // If the potentiometer is still moving, send the change control
      if (midiPState[i] != midiCState[i]) {

        controlChange(midiCh, cc + i, midiCState[i]); //  (channel, CC number,  CC value)
        MidiUSB.flush();


        //Serial.println(midiCState);
        potPState[i] = potCState[i]; // Stores the current reading of the potentiometer to compare with the next
        midiPState[i] = midiCState[i];
      }
    }
  }

}

// use if using with ATmega32U4 (micro, pro micro, leonardo...)

  /////////////////////////////////////////////
  // Arduino (pro)micro midi functions MIDIUSB Library
  void noteOn(byte channel, byte pitch, byte velocity) {
  midiEventPacket_t noteOn = {0x09, 0x90 | channel, pitch, velocity};
  MidiUSB.sendMIDI(noteOn);
  }

  void noteOff(byte channel, byte pitch, byte velocity) {
  midiEventPacket_t noteOff = {0x08, 0x80 | channel, pitch, velocity};
  MidiUSB.sendMIDI(noteOff);
  }

  void controlChange(byte channel, byte control, byte value) {
  midiEventPacket_t event = {0x0B, 0xB0 | channel, control, value};
  MidiUSB.sendMIDI(event);
  }

